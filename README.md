#基于Fly社区模板 + ASP.NET MVC + MySQL + NPoco 开发的轻量级社区

##功能列表：
### 游客
*登录
*注册
*查看帖子
*查看评论

###普通会员 --未激活邮箱的会员
拥有游客所有功能
*查看个人信息
*修改个人信息
*帖子点赞

###激活邮箱的会员
*发帖
*回帖
*编辑贴子

###管理员
拥有游客和会员的所有功能
*删除帖子
*帖子置顶
*帖子加精


演示地址:[http://fly.zhengjinfan.cn](http://fly.zhengjinfan.cn)
项目开源地址:[https://git.oschina.net/besteasyteam/Fly](https://git.oschina.net/besteasyteam/Fly)
Fly社区模板地址:[http://fly.layui.com/jie/4536.html](http://fly.layui.com/jie/4536.html)

交流QQ群：248049395


Author:Beginner
Email:zheng_jinfan@126.com




#更新日志

版本号：*1.0.0* 
更新时间：2017-03-01

##更新说明：
1.完成 *v1.0.0* 功能开发