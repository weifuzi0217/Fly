﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Fly.Web.Controllers
{
    public class ErrorController : BaseController
    {
        // GET: 404
        [AllowAnonymous]
        public ActionResult NotFound()
        {
            return View();
        }
        //500
        [AllowAnonymous]
        public ActionResult Error500()
        {
            return View();
        }
    }
}