﻿using Fly.Domain.Services;
using Fly.Domain.Services.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Fly.Web.Controllers
{
    public class HomeController : BaseController
    {
        // GET: Home
        [AllowAnonymous]
        public ActionResult Index()
        {
            //获取第一页的三十条数据
            var request = new GetPagePostsRequest(1, 30);
            var response = new PostService().GetPagePostsByFilter(request);
            return View(response);
        }
        //月雷锋榜Top12
        [AllowAnonymous]
        public PartialViewResult LoadContributionTopTwelve()
        {
            var response = new ContributionService().GetTopTwelve();
            return PartialView("_Contribution", response);
        }
        //友情链接
        [AllowAnonymous]
        public PartialViewResult LoadLinks()
        {
            var response = new LinkService().GetLinks();
            return PartialView("_Link", response.Items);
        }
    }
}