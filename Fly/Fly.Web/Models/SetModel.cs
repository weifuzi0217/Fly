﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fly.Web.Models
{
    public class SetModel
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Nickname { get; set; }
        public int Gender { get; set; }
        public string City { get; set; }
        public string Sign { get; set; }
    }
}