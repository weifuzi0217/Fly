﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fly.Web.Models
{
    public class RepassModel
    {
        public string NowPass { get; set; }

        public string Pass { get; set; }
    }
}