﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NPoco;
using Fly.Domain.Infrastructure;

namespace Fly.Domain.Services
{
    public class BaseService<T,TId> where T : BaseEntity<TId>
    {
        protected Database DbBase { get;}

        public BaseService() {
            DbBase = new Database("FlyConnectionString");
        }

        /// <summary>
        /// 验证数据的正确性，入库的最后一道关卡
        /// </summary>
        /// <param name="entity">实体</param>
        protected void ThrowExceptionIfEntityIsInvalid(T entity)
        {
            if (entity.GetBrokenRules().Any())
            {
                var brokenRules = new StringBuilder();
                brokenRules.AppendLine("数据验证不通过，错误信息：");
                foreach (var businessRule in entity.GetBrokenRules())
                {
                    brokenRules.AppendLine(businessRule.Rule);
                }
                throw new Exception(brokenRules.ToString());
            }
        }


        protected bool IsInsertSuccess(object val)
        {
            return val != null && Convert.ToInt32(val) > 0;
        }

        protected bool IsUpdateSuccess(int val)
        {
            return val > 0;
        }

        public Response Insert(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));
            var response = new Response();

            ThrowExceptionIfEntityIsInvalid(entity);

            var result = DbBase.Insert(entity);
            if(result!=null && Convert.ToInt32(result) > 0)
            {
                response.IsSuccess = true;
                response.Message = "操作成功";
                return response;
            }

            response.Message = "操作失败";
            return response;
        }
    }


}
