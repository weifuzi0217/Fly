﻿using Fly.Domain.Infrastructure;
using NPoco;
using System;

namespace Fly.Domain.Models
{
    /// <summary>
    /// 帖子类型
    /// </summary>
    [TableName("PostCategories")]
    [PrimaryKey("Id")]
    public class PostCategory : BaseEntity<int>
    {
        /// <summary>
        /// 帖子名称
        /// </summary>
        public string CategoryName { get; set; }

        protected override void Validate()
        {
            throw new NotImplementedException();
        }
    }
}
