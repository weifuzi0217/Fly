﻿using Fly.Domain.Infrastructure;
using System;
using NPoco;
using Fly.Domain.Helper;
using Fly.Domain.Services;

namespace Fly.Domain.Models
{
    /// <summary>
    /// 评论实体
    /// </summary>
    [TableName("Comments")]
    [PrimaryKey("Id")]
    public class Comment : BaseEntity<int>
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public int UserId { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "UserId", ReferenceMemberName = "Id")]
        public User User { get; set; }
        /// <summary>
        /// 帖子Id
        /// </summary>
        public int PostId { get; set; }


        [Reference(ReferenceType.OneToOne, ColumnName = "PostId", ReferenceMemberName = "Id")]
        public Post Post { get; set; }
        /// <summary>
        /// 点赞数
        /// </summary>
        public int Like { get; set; }
        /// <summary>
        /// 评论内容
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 评论时间
        /// </summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 是否采纳当前评论
        /// </summary>
        public bool IsAdopt { get; set; }
        /// <summary>
        /// 回复的时间戳
        /// </summary>
        public long Ticks { get; set; }

        [Ignore]
        public bool IsLike { get; set; }

        /// <summary>
        /// 获取文字描述的时间
        /// </summary>
        [Ignore]
        public string TimeAgo
        {
            get
            {
                return DateHelper.TimeAgoStr(DateTime.Now, CreateTime);
            }
        }

        protected override void Validate()
        {
            if (string.IsNullOrEmpty(Content))
                AddBrokenRule(new BusinessRule(nameof(Content), "回复内容不能为空"));
        }
    }
}
