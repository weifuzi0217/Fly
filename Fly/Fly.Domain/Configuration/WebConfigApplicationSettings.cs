﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fly.Domain.Configuration
{
    public class WebConfigApplicationSettings
    {
        public static string GetAppSettings(string key, string defaultVal = "")
        {
            var value = System.Configuration.ConfigurationManager.AppSettings[key];
            return value == null ? defaultVal : value;
        }
    }
}
