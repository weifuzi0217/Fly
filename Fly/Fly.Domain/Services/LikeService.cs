﻿using Fly.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fly.Domain.Services
{
    /// <summary>
    /// zan
    /// </summary>
    public class LikeService : BaseService<Like, int>
    {
        /// <summary>
        /// 是否已赞
        /// </summary>
        /// <param name="commentId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool ZanIsExists(int commentId, int userId)
        {
            return DbBase.Query<Like>().Any(p => p.CommentId == commentId && p.UserId == userId);
        }
    }
}
