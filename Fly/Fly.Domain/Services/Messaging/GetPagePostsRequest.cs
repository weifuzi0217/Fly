﻿using Fly.Domain.Infrastructure;
using Fly.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fly.Domain.Services.Messaging
{
    public class GetPagePostsRequest : RequestBaseOfPaging
    {
        public GetPagePostsRequest(int pageIndex, int pageSize) : base(pageIndex, pageSize)
        {
        }
        /// <summary>
        /// 帖子状态
        /// </summary>
        public PostStatus? PostStatus { get; set; }
        /// <summary>
        /// 是否为精贴
        /// </summary>
        public bool? IsBoutique { get; set; }
        /// <summary>
        /// 用户ID
        /// </summary>
        public int? UserId { get; set; }


    }
}
