﻿using Fly.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Fly.Domain.Extension;
using Fly.Domain.Infrastructure;
using Fly.Domain.Services.Messaging;

namespace Fly.Domain.Services
{
    /// <summary>
    /// 帖子服务类
    /// </summary>
    public class PostService : BaseService<Post, int>
    {
        /// <summary>
        /// 帖子设置
        /// </summary>
        /// <param name="postId">帖子id</param>
        /// <param name="status">状态</param>
        /// <param name="field">设置值 置顶--加精</param>
        /// <returns></returns>
        public Response PostSet(int postId, int status, string field,int userId=0)
        {
            var response = new Response();
            var post = DbBase.SingleOrDefaultById<Post>(postId);
            if (post == null)
            {
                response.Message = "非法操作";
                return response;
            }

            switch (field)
            {
                //加精  -- 取消加精
                case "status":
                    post.IsBoutique = status == 1;
                    var r1 = DbBase.UpdateMany<Post>().OnlyFields(p => p.IsBoutique).Where(p => p.Id == postId).Execute(post);
                    var message = new Message
                    {
                        ToId = post.UserId,
                        FormId = userId,
                        Href = "post/" + postId,
                        Content = post.Title,
                        MessageType = MessageType.Boutique,
                        CreateTime = DateTime.Now
                    };
                    DbBase.Insert(message);//添加一条消息 

                    if (IsUpdateSuccess(r1))
                    {
                        response.IsSuccess = true;
                        response.Message = "操作成功";
                        return response;
                    }
                    break;
                //置顶  -- 取消置顶
                case "stick":
                    post.IsTop = status == 1;
                    var r2 = DbBase.UpdateMany<Post>().OnlyFields(p => p.IsTop).Where(p => p.Id == postId).Execute(post);
                    if (IsUpdateSuccess(r2))
                    {
                        response.IsSuccess = true;
                        response.Message = "操作成功";
                        return response;
                    }
                    break;
            }
            response.Message = "操作失败";
            return response;
        }

        /// <summary>
        /// 获取最近热议Top15
        /// </summary>
        /// <returns></returns>
        public GetListsResponse<Post> GetHotCommentPostsTopFifteen()
        {
            var response = new GetListsResponse<Post>();
            var result = DbBase.Query<Post>().OrderByDescending(p => p.CommentCount).ToPage(1, 15);
            if (result.Items != null && result.Items.Count > 0)
            {
                response.IsSuccess = true;
                response.Message = "获取成功";
                response.Items = result.Items;
                return response;
            }
            response.Message = "暂无数据";
            return response;
        }
        /// <summary>
        /// 根据用户id获取帖子总数
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int GetPostCountByUserId(int userId)
        {
            return DbBase.Query<Post>().Count(p => p.UserId == userId);
        }
        /// <summary>
        /// 检查是否有权限编辑改帖子
        /// </summary>
        /// <param name="userId">用户id</param>
        /// <param name="postId">帖子id</param>
        /// <returns></returns>
        public bool CheckIsEdit(int userId, int postId)
        {
            return DbBase.Query<Post>().Any(p => p.UserId == userId && p.Id == postId);
        }
        /// <summary>
        /// 浏览量+1
        /// </summary>
        /// <param name="entity"></param>
        public void HitsPlusOne(Post entity)
        {
            if (entity != null)
            {
                entity.Hits++;
                DbBase.UpdateMany<Post>().OnlyFields(p => p.Hits).Where(p => p.Id == entity.Id).Execute(entity);
            }
        }
        /// <summary>
        /// 获取单篇帖子
        /// </summary>
        /// <param name="postId"></param>
        /// <returns></returns>
        public Post GetPostById(int postId)
        {
            return DbBase.Query<Post>().Include(p => p.User).Include(p => p.PostCategory).SingleOrDefault(p => p.Id == postId);
        }
        /// <summary>
        /// 获取帖子列表
        /// </summary>
        /// <param name="request">搜索条件</param>
        /// <returns></returns>
        public GetPagingResponse<Post> GetPagePostsByFilter(GetPagePostsRequest request)
        {
            var response = new GetPagingResponse<Post>();

            var result = DbBase.Query<Post>()
                .Include(p => p.User) //联表查询
                .Include(p => p.PostCategory)
                .HasWhere(request.IsBoutique, p => p.IsBoutique)
                .HasWhere(request.PostStatus, p => p.PostStatus == request.PostStatus)
                .HasWhere(request.UserId, p => p.UserId == request.UserId)
                .Where(p => p.IsShow) //查询状态为显示的数据
                .OrderByDescending(p => p.IsTop)  //排序方式：倒序，顺序：是否置顶->排序值->创建时间
                .ThenByDescending(p => p.Sort)
                .ThenByDescending(p => p.CreateTime)
                .ToPage(request.PageIndex, request.PageSize);

            if (result.Items != null && result.Items.Count > 0)
            {
                response.IsSuccess = true;
                response.Message = "获取成功!";
                response.Pages = result;
                return response;
            }
            response.Message = "暂无数据!";
            return response;
        }
        /// <summary>
        /// 发表帖子
        /// </summary>
        /// <param name="entity">实体</param>
        /// <returns></returns>
        public Response SendPost(Post entity)
        {
            return InsertOrUpdate(entity, true);
        }
        /// <summary>
        /// 更新帖子信息
        /// </summary>
        /// <param name="entity">实体</param>
        /// <returns></returns>
        public Response UpdatePost(Post entity)
        {
            return InsertOrUpdate(entity, false);
        }
        /// <summary>
        /// 删除帖子
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Response DeletePost(int id)
        {
            if (id == 0)
            {
                throw new ArgumentNullException(nameof(id));
            }
            var response = new Response();

            using (var tran = DbBase.GetTransaction())
            {
                //删除帖子
                var r1 = DbBase.Delete<Post>(id);
                //删除评论
                var r2 = DbBase.DeleteMany<Comment>().Where(p => p.PostId == id).Execute();

                tran.Complete();
                if (IsUpdateSuccess(r1))
                {
                    response.IsSuccess = true;
                    response.Message = "操作成功";
                    return response;
                }
            }

            response.Message = "操作失败";
            return response;
        }
        /// <summary>
        /// 获取最近热贴Top15
        /// </summary>
        /// <returns></returns>
        public GetListsResponse<Post> GetHotPostsTopFifteen()
        {
            var response = new GetListsResponse<Post>();

            var result = DbBase.Query<Post>().OrderByDescending(p => p.Hits).ToPage(1, 15);
            if (result.Items != null && result.Items.Count > 0)
            {
                response.IsSuccess = true;
                response.Message = "获取成功";
                response.Items = result.Items;
                return response;
            }
            response.Message = "暂无数据";
            return response;
        }


        //新增或修改
        private Response InsertOrUpdate(Post entity, bool isInsert)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            var response = new Response();

            ThrowExceptionIfEntityIsInvalid(entity);//验证数据的正确性，入库的最后一道关卡

            bool flag = false;
            if (isInsert)
            {
                var user = DbBase.SingleOrDefaultById<User>(entity.UserId);
                if (user == null)
                {
                    response.Message = "非法操作，请重新登录重试！";
                    return response;
                }
                if (user.Integral < entity.Reward)
                {
                    response.Message = "飞吻余额不足，发布失败！";
                    return response;
                }
                using (var tran = DbBase.GetTransaction())
                {
                    user.Integral -= entity.Reward;
                    DbBase.UpdateMany<User>().OnlyFields(p => p.Integral).Where(p => p.Id == entity.UserId).Execute(user);//只更新积分
                    var result = DbBase.Insert(entity);
                    flag = result != null && Convert.ToInt32(result) > 0;

                    tran.Complete();
                }

            }
            else
            {
                var result = DbBase.Update(entity);
                flag = result > 0;
            }

            if (flag)
            {
                response.IsSuccess = true;
                response.Message = "发布成功";
                return response;
            }
            response.Message = "发布失败!";
            return response;
        }
    }
}
