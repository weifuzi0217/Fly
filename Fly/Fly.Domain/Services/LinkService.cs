﻿using Fly.Domain.Infrastructure;
using Fly.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fly.Domain.Services
{
    public class LinkService : BaseService<Link, int>
    {
        /// <summary>
        /// 读取友情链接列表
        /// </summary>
        /// <returns></returns>
        public GetListsResponse<Link> GetLinks()
        {
            var response = new GetListsResponse<Link>();
            var result = DbBase.Query<Link>().OrderByDescending(p=>p.Sort).ToList();
            if (result != null && result.Count > 0)
            {
                response.IsSuccess = true;
                response.Message = "获取成功";
                response.Items = result;
                return response;
            }
            response.Message = "暂无数据";
            return response;
        }
    }
}
